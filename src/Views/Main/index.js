import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { Title, Cont, Card, SendButton, MyInput } from "./styles";
import api from "../../services/apiCards";

import { TextField } from '@material-ui/core';
import DeleteIcon from "@material-ui/icons/Delete";
import CardItem from '../../components/CardItem';
import PersonItem from '../../components/PersonItem';
import * as personActions from "../../store/modules/person/actions";

import PersonAddIcon from '@material-ui/icons/PersonAdd';
import IconButton from "@material-ui/core/IconButton";
import Button from '@material-ui/core/Button';
import PostAddIcon from '@material-ui/icons/PostAdd';
import Paper from '@material-ui/core/Paper';


class Main extends Component{
  state = {
    persons: ["Marcelo", "Duda", "Pedro"],
    newPerson: '',
    cards: [],
  };

  async componentDidMount() {
    const { user } = this.props;
    if (user.length > 0 ) {
      const response = await api.get("/cards", {
      headers: { Authorization: `Bearer ${user[0].token}`},
    });

    this.setState({ cards: response.data });

    console.log(response.data);
    }
  };

  componentDidUpdate(prevProps, prevState) {};//executa quando altera o state local

  handleInputChange = e => {
    this.setState({newPerson: e.target.value});
  }

  handleLogout = event => {
    localStorage.removeItem('persist:react-cards');
    window.location.reload();
  }


  handleSubmit = async event => {
    event.preventDefault();

    const { login } = this.props;

    const response = await api.post('/login', {
      email: this.state.login,
      password: this.state.password
    });

    const { user, token } = response.data;

    user.token = token;
    login(user);

  };

  sendToStore = (person) => {
    const { addToPerson } = this.props;
    addToPerson(person);
  }

  removeToStore = person => {
    const { removeFromPerson } = this.props;

    removeFromPerson(person);
  }

  handleDelete = async id => {
    const {user} = this.props;
    const response = await api.delete("/cards/${id}",{
        headers: { Authorization: `Bearer ${user[0].token}`},
      });

    this.setState({
      cards: this.state.cards.filter(item => item.id !== id),
    })
  };

  render() {
    return (
      <Card>
      <Button variant="outlined" onClick={this.handleLogout}>Logout</Button>

      <Title>Recados <PostAddIcon color="secundary" style={{ fontSize: 50 }}/></Title>
        <Cont>
        <ul>
          {this.state.cards.map(item =>(
            <li>
              <Paper>
              <h3>{item.title}</h3>
              <p>{item.content}</p>
              <IconButton onClick={this.handleDelete} aria-label="delete">
                <DeleteIcon />
              </IconButton>
              </Paper>
            </li>
            ))}
        </ul>
        <br/>
        <form onSubmit={this.handleSubmit}>
        <TextField
        MyInput label="Digite o titulo:" name="title" type="text" onChange={this.handleSubmit}
        MyInput label="Digite o conteudo:" name="content" type="text" onChange={this.handleSubmit}
        value={this.state.newCard}
        />
        <IconButton onClick={this.handleSubmit} aria-label="salvar">
          <PersonAddIcon type="button" style={{ fontSize: 40 }}/>
      </IconButton>
        </form>
        </Cont>

      </Card>

    );
  }
}

const mapStateToProps = state => ({
  user: state.user,
});

const mapDispatchToProps = dispatch =>
bindActionCreators(personActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Main);
