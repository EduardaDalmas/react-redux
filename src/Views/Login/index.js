
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import React, { Component } from 'react';

import api from "../../services/apiCards";
import * as UserActions from "../../store/modules/user/actions";

import { Title, Card, SendButton, MyInput } from './styles';

class Login extends Component {
  state = {
    login: '',
    password: '',
  };

  componentDidMount() {

  }

  handleInputChange = event => {
    const { target } = event;

    if(target.name == 'email'){
      this.setState({ login: target.value });
    } else {
      this.setState({ password: target.value });
    }
  }

  componentDidUpdate(prevProps, prevState) {

  }

  handleSubmit = async event => {
    event.preventDefault();

    const { login } = this.props;

    const response = await api.post('/login', {
      email: this.state.login,
      password: this.state.password
    });

    const { user, token } = response.data;

    user.token = token;
    login(user);

  };

  render() {
    return (
      <>
        <Card>
          <center>
            <ul>
              <Title>Login</Title>
              <form onSubmit={this.handleSubmit}>
                <MyInput label="Digite seu email:" name="email" type="text" onChange={this.handleInputChange}/>
                <MyInput label="Digite sua senha:" name="password" type="password" onChange={this.handleInputChange}/>
                <SendButton type="submit">Enviar</SendButton>
              </form>
            </ul>
          </center>
        </Card>
      </>
    );
  }
}


const mapDispatchToProps = dispatch =>
bindActionCreators(UserActions, dispatch);

export default connect(null, mapDispatchToProps)(Login);

