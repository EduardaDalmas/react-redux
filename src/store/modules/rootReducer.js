import { combineReducers } from "redux";

import person from "./person/reducer";
import auth from "./auth/reducer";
import user from "./user/reducer";

export default combineReducers({
  person,
  auth,
  user,
});
