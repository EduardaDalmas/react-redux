export function login(auth) {
  return {
    type: "@auth/LOGIN",
    auth,
  };
}
