export default function auth(state = [], action) {
  switch (action.type) {
    case "@auth/LOGIN":
      return [...state, action.auth];

    default:
      return state;
  }
}
