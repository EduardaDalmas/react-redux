import React from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as personActions from "../../store/modules/person/actions";

import { Nav } from "./styles";

function Header({ personCount, showAlertTotal }) {
  return (
    <Nav>
      Total: {personCount}
      <button onClick={() => showAlertTotal()} type="button">
        Click
      </button>
    </Nav>
  );
}

const mapStateToProps = state => ({
  personCount: state.person.length,
});

const mapDispatchToProps = dispatch =>
bindActionCreators(personActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
