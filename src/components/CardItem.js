import React from "react";
import PropTypes from "prop-types";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";

function CardItem({ card, onDelete }) {
  return (
    <li key={card}>
      <card>{card}</card>
      <IconButton onClick={onDelete} aria-label="delete">
        <DeleteIcon />
      </IconButton>
    </li>
  );
}

CardItem.defaultProps = {
  card: "without card.",
};

CardItem.propTypes = {
  card: PropTypes.string,
  onDelete: PropTypes.func.isRequired,
};

export default CardItem;
